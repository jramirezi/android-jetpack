package com.example.apparguments.view;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.example.apparguments.R;

public class ListFragmentDirections {
  private ListFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionListFragmentToDetailFragment() {
    return new ActionOnlyNavDirections(R.id.action_listFragment_to_detailFragment);
  }
}
